package server

import (
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/NeonDark/alethia/cliutil"
	"log/slog"
	"net/http"
	"strconv"
	"sync"
	"time"
)

var thisLogger *slog.Logger

// InitLogger creates new loggers with the given parameters.
func InitLogger() {
	thisLogger = slog.With(slog.String("module", "server"))
}

func info(msg string, v ...any) {
	thisLogger.Info(msg, v...)
}

func warn(err error, v ...any) {
	cliutil.LogError(thisLogger, err, v...)
}

// StartMetrics creates a metrics server on the given port
func StartMetrics(wg *sync.WaitGroup, port uint) *http.Server {
	handler := http.NewServeMux()
	handler.Handle(routeMetrics, adapt(promhttp.Handler(), routeMetrics,
		limitMethod("GET"), maxBody()))

	// create server
	srv := &http.Server{
		Addr:              ":" + strconv.FormatUint(uint64(port), 10),
		Handler:           handler,
		ReadTimeout:       time.Minute,
		ReadHeaderTimeout: time.Second * 5,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			warn(cliutil.NewStackError(err))
		}
		wg.Done()
	}()

	info(fmt.Sprintf("Started metrics server at endpoint http://localhost%s", srv.Addr))

	return srv
}
