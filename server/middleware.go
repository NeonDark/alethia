package server

import (
	"gitlab.com/NeonDark/alethia/cliutil"
	"net/http"
)

// maxBodySize is the maximum number of bytes a body can contain
// without an error being thrown while it being read
const maxBodySize = 5242880 // 5242880 = 1024 * 1024 * 5 -> 5 MiB

type adapter func(http.Handler, string) http.Handler

func adapt(h http.Handler, route string, adapters ...adapter) http.Handler {
	for i := len(adapters) - 1; i >= 0; i-- {
		h = adapters[i](h, route)
	}
	return h
}

// maxBody limits the amount of bytes which can be read from the request body to maxBodySize.
func maxBody() adapter {
	return func(h http.Handler, route string) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.Body = http.MaxBytesReader(w, r.Body, maxBodySize)
			h.ServeHTTP(w, r)
		})
	}
}

func limitMethod(method string) adapter {
	return func(h http.Handler, route string) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method != method {
				warn(cliutil.NewStackErrorf("error received %s request for route %s instead of %s", r.Method, route, method))
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}

			h.ServeHTTP(w, r)
		})
	}
}
