package collector

import (
	"crypto/tls"
	"encoding/csv"
	"fmt"
	irc "github.com/fluffle/goirc/client"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/NeonDark/alethia/cliutil"
	"log/slog"
	"regexp"
	"strings"
	"time"
)

var thisLogger *slog.Logger

// InitLogger creates new loggers with the given parameters.
func InitLogger() {
	thisLogger = slog.With(slog.String("module", "collector"))
}

//func info(msg string, v ...any) {
//	thisLogger.Info(msg, v...)
//}

func warn(err error, v ...any) {
	cliutil.LogError(thisLogger, err, v...)
}

func getIRCConfig(host string, port uint) *irc.Config {
	cfg := irc.NewConfig("lmslod", "lmslod", "lmslod")
	cfg.SSL = true
	cfg.SSLConfig = &tls.Config{ServerName: host, MinVersion: tls.VersionTLS12}
	cfg.Server = fmt.Sprintf("%s:%d", host, port)
	cfg.Version = "1.0"
	cfg.QuitMessage = ""

	return cfg
}

type Config struct {
	Host             string
	Port             uint
	Channel          string
	FragmentCount    int
	AllowRegex       string
	DenyRegex        string
	RemoveCharacters string
	Writer           *csv.Writer
}

type Collector struct {
	irc                  *irc.Conn
	ServerSentDisconnect chan bool
	totalLines           prometheus.Counter
	collectedLines       prometheus.Counter
	writer               *csv.Writer
	allow                *regexp.Regexp
	deny                 *regexp.Regexp
	charactersToRemove   string
	fragmentCount        int
}

func (c *Collector) Connect() error {
	if err := c.irc.Connect(); err != nil {
		return cliutil.NewStackError(err)
	}

	return nil
}

func (c *Collector) Close() error {
	if err := c.irc.Close(); err != nil {
		return cliutil.NewStackError(err)
	}

	return nil
}

func NewCollector(config Config) (*Collector, error) {
	allowRegex, denyRegex, err := buildFilters(config.AllowRegex, config.DenyRegex)
	if err != nil {
		return nil, cliutil.NewStackError(err)
	}

	c := irc.Client(getIRCConfig(config.Host, config.Port))

	// disconnect signal
	ch := make(chan bool, 1)
	collector := Collector{
		irc:                  c,
		ServerSentDisconnect: ch,
		allow:                allowRegex,
		deny:                 denyRegex,
		charactersToRemove:   config.RemoveCharacters,
		fragmentCount:        config.FragmentCount,
		writer:               config.Writer,
		totalLines: prometheus.NewCounter(prometheus.CounterOpts{
			Name: "alethia_lines_total",
			Help: "The total number of lines",
		}),
		collectedLines: prometheus.NewCounter(prometheus.CounterOpts{
			Name: "alethia_collected_lines_total",
			Help: "The total number of lines collected",
		})}

	// set which channel to join when connecting
	c.HandleFunc(irc.CONNECTED, func(conn *irc.Conn, line *irc.Line) { conn.Join(config.Channel) })
	c.HandleFunc(irc.DISCONNECTED, func(conn *irc.Conn, line *irc.Line) { ch <- true })
	c.HandleFunc(irc.PRIVMSG, collector.handleNewMessage)

	if err := c.Connect(); err != nil {
		return nil, cliutil.NewStackError(err)
	}

	return &collector, nil
}

func (c *Collector) handleNewMessage(_ *irc.Conn, line *irc.Line) {
	c.totalLines.Inc()
	text := line.Text()

	if !checkLine(text, c.allow, c.deny) {
		return
	}

	fragments := strings.Fields(text)

	if len(fragments) != c.fragmentCount {
		return
	}

	if err := c.writer.Write([]string{line.Time.Format(time.RFC3339),
		removeCharacters(fragments[1], c.charactersToRemove), fragments[2]}); err != nil {
		warn(err)
	}
	c.writer.Flush()
	c.collectedLines.Inc()
}

func checkLine(line string, allow *regexp.Regexp, deny *regexp.Regexp) bool {
	return allow.MatchString(line) && !deny.MatchString(line)
}

func buildFilters(allow string, deny string) (*regexp.Regexp, *regexp.Regexp, error) {
	a, err := regexp.Compile(allow)
	if err != nil {
		return nil, nil, cliutil.NewStackError(err)
	}

	d, err := regexp.Compile(deny)
	if err != nil {
		return nil, nil, cliutil.NewStackError(err)
	}

	return a, d, nil
}

func removeCharacters(line string, removeList string) string {
	if removeList == "" {
		return line
	}

	if line == "" {
		return ""
	}

	var cleanedString string
	for _, lineCharacter := range line {
		inRemoveList := false
		for _, r := range removeList {
			if r == lineCharacter {
				inRemoveList = true
				break
			}
		}

		if !inRemoveList {
			cleanedString += string(lineCharacter)
		}
	}

	return cleanedString
}
