package collector

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_removeCharacters(t *testing.T) {
	tests := []struct {
		line       string
		removeList string
		want       string
	}{
		{
			line:       "",
			removeList: "",
			want:       "",
		},
		{
			line:       "abc.dde{asdf]asdfl;[",
			removeList: "",
			want:       "abc.dde{asdf]asdfl;[",
		},
		{
			line:       "abc.dde{asdf]asdfl;[",
			removeList: "[];.",
			want:       "abcdde{asdfasdfl",
		},
	}
	for _, tt := range tests {
		require.Equal(t, tt.want, removeCharacters(tt.line, tt.removeList))
	}
}

func Test_buildFilters(t *testing.T) {
	tests := []struct {
		allow   string
		deny    string
		wantErr bool
	}{
		{
			allow:   "^.*((AAA)|(aaa)).*$",
			deny:    "^.*((asdf)|(test)).*$",
			wantErr: false,
		},
		{
			allow:   "",
			deny:    "",
			wantErr: false,
		},
		{
			allow:   "?",
			deny:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		_, _, err := buildFilters(tt.allow, tt.deny)
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
		}
	}
}

func Test_checkLine(t *testing.T) {
	allow, deny, err := buildFilters("(?i)^.*\\[.*(AAA).*\\].*$", "(?i)^.*((test)|(\\[.*img.*\\])).*$")
	require.NoError(t, err)

	tests := []struct {
		line   string
		result bool
	}{
		{line: "sasdfd[saaasdf] asdf[img]", result: false},
		{line: "sasdfd[saaaasdf] asdf", result: true},
		{line: "sasdfd[saaaasdf] testasdf", result: false},
		{line: "sasdfd[sAAAasdf] asdf[img]", result: false},
		{line: "sasdfd[sAAAasdf] asdf[IMG]", result: false},
	}
	for _, tt := range tests {
		require.Equal(t, tt.result, checkLine(tt.line, allow, deny), tt.line)
	}
}
