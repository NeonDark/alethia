package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"gitlab.com/NeonDark/alethia/cliutil"
	"gitlab.com/NeonDark/alethia/collector"
	"gitlab.com/NeonDark/alethia/server"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func main() {
	////// SET FLAGS //////
	const defaultConfigName = "config.yml"
	var filePath string
	var createConfigFile bool
	cliutil.SetConfigFlags(defaultConfigName, &filePath, &createConfigFile)
	flag.Parse()

	////// CONFIGURATION FILE HANDLING //////

	if createConfigFile {
		fmt.Println("Generating configuration file ...")
		err := cliutil.WriteConfig(defaultConfigName, defaultConfig)
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Println("config file", defaultConfigName, "successfully created")
		return
	}

	var config Config
	if err := cliutil.ReadConfig(filePath, &config); err != nil {
		fmt.Println(err)
		return
	}

	////// SETUP //////

	// setup Logging
	f, err := cliutil.GetLogfile(config.Logfile)
	if err == nil {
		defer func() {
			if err = f.Close(); err != nil {
				fmt.Println(err)
			}
		}()
	} else if len(config.Logfile) > 0 {
		fmt.Println("Could not create logfile", config.Logfile)
		return
	}

	initAllLoggers(f)

	csvFile, err := os.OpenFile(config.Collector.CsvFileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		warn(err)
		return
	}

	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			warn(err)
		}
	}(csvFile)

	csvWriter := csv.NewWriter(csvFile)
	csvWriter.Comma = ';'

	info("connecting to IRC server", "host", config.IRC.Host, "port", config.IRC.Port, "channel", config.IRC.Channel)

	c, err := collector.NewCollector(collector.Config{
		Host:             config.IRC.Host,
		Port:             config.IRC.Port,
		Channel:          config.IRC.Channel,
		AllowRegex:       config.Collector.AllowRegex,
		DenyRegex:        config.Collector.DenyRegex,
		RemoveCharacters: config.Collector.RemoveCharacters,
		FragmentCount:    config.Collector.FragmentCount,
		Writer:           csvWriter,
	})
	if err != nil {
		warn(err)
		return
	}

	var wg sync.WaitGroup

	wg.Add(1)
	metricsHTTPServer := server.StartMetrics(&wg, 8481)

	chSignal := make(chan os.Signal, 1)
	signal.Notify(chSignal, os.Interrupt, syscall.SIGTERM)

	// main loop of the program
	execLoop(chSignal, c)

	// stop metrics server
	shutdownServer(metricsHTTPServer)

	wg.Wait()
}

func execLoop(termSignal chan os.Signal, c *collector.Collector) {
	for {
		select {
		case <-termSignal:
			info("interrupted. exiting ...")
			if err := c.Close(); err != nil {
				warn(err)
			}
			return
		case <-c.ServerSentDisconnect:
			info("received disconnect from server, reconnecting ...")
			if err := c.Connect(); err != nil {
				warn(err)
				return
			}
		}
	}
}
