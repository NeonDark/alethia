package main

import (
	"context"
	"gitlab.com/NeonDark/alethia/cliutil"
	"gitlab.com/NeonDark/alethia/collector"
	"gitlab.com/NeonDark/alethia/server"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"
)

var thisLogger *slog.Logger

func initAllLoggers(fileHandle *os.File) {
	var outputWriter io.Writer
	if fileHandle != nil {
		outputWriter = io.MultiWriter(fileHandle, os.Stdout)
	} else {
		outputWriter = os.Stdout
	}

	logger := slog.New(slog.NewTextHandler(outputWriter, nil))
	slog.SetDefault(logger)

	thisLogger = slog.With(slog.String("module", "main"))

	collector.InitLogger()
	server.InitLogger()
}

func info(msg string, v ...any) {
	thisLogger.Info(msg, v...)
}

func warn(err error, v ...any) {
	cliutil.LogError(thisLogger, err, v...)
}

type IRCConfig struct {
	Host    string `yaml:"host"`
	Port    uint   `yaml:"port"`
	Channel string `yaml:"channel"`
}

type Collector struct {
	CsvFileName      string `yaml:"csvFileName"`
	FragmentCount    int    `yaml:"fragmentCount"`
	AllowRegex       string `yaml:"allowRegex"`
	DenyRegex        string `yaml:"denyRegex"`
	RemoveCharacters string `yaml:"removeCharacters"`
}

type Config struct {
	Logfile   string    `yaml:"logfile"`
	IRC       IRCConfig `yaml:"irc"`
	Collector Collector `yaml:"collector"`
}

var defaultConfig = Config{
	Logfile: "alethia.log",
	IRC: IRCConfig{
		Host: "",
		Port: 7000,
	},
	Collector: Collector{
		FragmentCount:    3,
		AllowRegex:       "",
		DenyRegex:        "",
		RemoveCharacters: "",
	},
}

// shutdownServer sends a shutdown signal to the server with a timout of 10 seconds
func shutdownServer(srv *http.Server) {
	if srv == nil {
		return
	}
	info("Shutting down server")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer func() {
		// extra handling here
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		warn(cliutil.NewStackErrorf("Server was shutdown and returned error: %w", err))
	}
}
