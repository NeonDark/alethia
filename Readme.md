# Alethia

Collects IRC messages

## Commandline Arguments

| Flag         | Default Value |                                    Description |
|--------------|:-------------:|-----------------------------------------------:|
| createConfig |     false     | creates a default config file (default: false) |
| config       |  config.yml   |         config file path (default: config.yml) |
