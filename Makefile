# credits: https://betterprogramming.pub/my-ultimate-makefile-for-golang-projects-fcc8ca20c9bb
GOCMD=go
GOTEST=$(GOCMD) test
GOVET=$(GOCMD) vet
BINARY_NAME=alethia
VERSION?=1.2.11
SERVICE_PORT?=3000
DOCKER_REGISTRY=bluedark/

GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

.PHONY: all test build docker-build docker-release help vet update_dependencies tidy lint swagger-create swagger-fmt push-tag

all: help

## Build:
build: ## Build your project and put the output binary in out/bin/
	mkdir -p build
	$(GOCMD) build -o build/$(BINARY_NAME) ./cmd

clean: ## Remove build related file
	rm -rf ./build

## Test:
test: ## Run the tests of the project
	$(GOTEST) -cover -race ./...

## Vet:
vet: ## Run the tests of the project
	$(GOCMD) vet ./...

coverage: ## Run the tests of the project and export the coverage
	$(GOTEST) -cover -covermode=count -coverprofile=profile.cov ./...
	$(GOCMD) tool cover -func profile.cov

## Docker:
docker-build: ## Use the dockerfile to build the container
	docker build --rm --tag $(DOCKER_REGISTRY)$(BINARY_NAME):latest .
	docker tag $(DOCKER_REGISTRY)$(BINARY_NAME) $(DOCKER_REGISTRY)$(BINARY_NAME):$(VERSION)

docker-release: docker-build ## Build and release the container with tag latest and version
	# Push the docker images
	docker push $(DOCKER_REGISTRY)$(BINARY_NAME):latest
	docker push $(DOCKER_REGISTRY)$(BINARY_NAME):$(VERSION)

update_dependencies:
	$(GOCMD) get -u ./...

tidy: ## Run go mod tidy on the default go.mod file and bitcoin go.mod file
	$(GOCMD) mod tidy

lint:
	golangci-lint run

swagger-create: ## Creates openapi docs
	./swag init --pd -d cmd,server -o openapi

swagger-fmt: ## Formats swagger annotations
	./swag fmt -d cmd,server

push-tag: ## increment the most recent tag and push it and any local commits
	bash createTag.sh

## Help:
help: ## Show this help.
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)